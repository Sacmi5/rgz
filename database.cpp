#include "database.h"
#include <iterator>
#include <iostream>
#include <vector>
#include <map>
#include <stdexcept>

using namespace std;

map<string, string> DbFile::operator[](const size_t &index) const {
    ifstream file(this->pathToFile);
    string line, cell;
    stringstream lineStream;
    size_t i = -1, count = 0;
    map<string, string> dict;

    getline(file,line);
    while (getline(file, line))
    {
        i++;

        if (i != index)
            continue;

        lineStream << line;
        while (getline(lineStream, cell, this->delimiter))
        {
            if (count == this->_head.size())
                break;

            dict.insert({this->_head[count], cell});
            count++;
        }

        break;
    }

    return dict;
}

size_t DbFile::size() const {
    return this->_size;
}

vector<string> DbFile::head() const {
    return this->_head;
}

ifstream DbFile::openFile(const string& path) {
    ofstream _file;
    ifstream file(path);

    if (!file.good())
    {
        _file.open(path);
        _file.close();
        file.open(path);
    }

    return file;
}

DbFile::DbFile(const string& pathToFile, const vector<string>& defaultHead, const char& delimiter) {
    auto file = this->openFile(pathToFile); // файл

    string line, cell;
    stringstream lineStream;
    map<string, string> lineData;
    size_t iter = 0;
    ofstream dst(pathToFile, ios::app);

    this->pathToFile = pathToFile;
    this->delimiter = delimiter;
    this->_size = 0;

    getline(file,line);
    lineStream << line;
    while (getline(lineStream, cell, delimiter)) {
        this->_head.push_back(cell);
    }
    lineStream.clear();

    if (this->_head.empty())
    {
        if (defaultHead.empty())
            throw invalid_argument("default header is not defined!");

        while (iter != defaultHead.size()) {
            if (iter != 0)
                dst << this->delimiter;

            dst << defaultHead[iter];
            iter++;
        }
        dst.close();

        this->_head = defaultHead;
    }

    while (getline(file,line))
        this->_size++;

    file.close();
}


void DbFile::push(const map<string, string>& line) {
    this->writeLine(this->_size, line);
    this->_size++;
}

void DbFile::pop(const size_t& index) {
    ofstream dst;
    ifstream src;
    int position;
    string line;

    if (index >= this->_size)
        return;

    DbFile::createTempFile(this->pathToFile, "_temp");

    src.open("_temp");
    dst.open(this->pathToFile, ios::trunc);

    for (position = -1; getline(src, line); position++) {
        if (position == index)
            continue;

        dst << line << endl;
    }

    this->_size--;
    src.close();
    dst.close();
}

int DbFile::getLineByValue(const string& column, const string& value) const {
    map<string, string> item;

    for (size_t i = 0; i < this->_size; i++)
    {
        item = this->operator[](i);

        if (item[column] == value)
            return i;
    }

    return -1;
}

vector<size_t> DbFile::find(const function<bool(map<string, string>)> &query) const {
    vector<size_t> items;

    for (size_t i = 0; i < this->_size; i++)
        if (query(this->operator[](i)))
            items.push_back(i);

    return items;
}

int DbFile::findOne(const function<bool(map<string, string>)> &query) const {
    for (size_t i = 0; i < this->_size; i++)
        if (query(this->operator[](i)))
            return i;

    return -1;
}

void DbFile::writeLine(const size_t &index, map<string, string> value) {
    ofstream dst;
    ifstream src;
    int position = -1;
    size_t iter = 0;
    string line;

    if (index > this->_size)
        return;

    DbFile::createTempFile(this->pathToFile, "_temp");

    src.open("_temp");
    dst.open(this->pathToFile, ios::trunc);

    for (position = -1; getline(src, line); ++position) {
        if (position == index)
            while (iter != this->_head.size()) {
                if (iter != 0)
                    dst << this->delimiter;

                dst << value[this->_head[iter]];
                iter++;
            }
        else
            dst << line;

        dst << endl;
    }

    if (this->_size == position)
        while (iter != this->_head.size()) {
            if (iter != 0)
                dst << this->delimiter;

            dst << value[this->_head[iter]];
            iter++;
        }

    src.close();
    dst.close();
}

void DbFile::createTempFile(const string &path, const string &tempName)  {
    ofstream dst(tempName, ios::binary);
    ifstream src(path, ios::binary);

    dst << src.rdbuf();
    dst.close();
    src.close();
}

int DbFile::getLast() const {
    return this->_size - 1;
}
