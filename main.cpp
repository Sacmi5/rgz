#include <locale>
#include <iostream>
#include <exception>
#include "database.h"
#include "headers.h"
#include "time.h"

using namespace std;

void clearCin()
{
    cin.clear();
    while (cin.get() != '\n');
}

int countLongInt(unsigned long long number)
{
    int count = 0;

    while(number){
        number /= 10;
        count++;
    }

    return count;
}

int _week = 0;

void writeMenu() {
    cout << "1. Регистрация пациента" << endl;
    cout << "2. Запись на прием" << endl;
    cout << "3. Добавление работника" << endl;
    cout << "4. Увольнение работника" << endl;
    cout << "5. Отправить в отпуск/больничный сотрудника" << endl;
    cout << "6. Показать график сотруднков" << endl;
    cout << "7. Показать график докторов" << endl;
    cout << "8. Редактировать график сотрудника" << endl;
    cout << "9. Обновить список отпусков" << endl;
    cout << "0. Завершить работу" << endl;
}

void registerPatients()
{
    string name;
    unsigned long long snils, oms;
    bool sex;
    map<string, string> data;
    DbFile dbFile("patients.csv", Headers::getPatients());

    cout << "Введите ФИО пациента:" << endl;
    clearCin();
    getline(cin, name);
    data.insert({"name", name});

    cout << "Введите ваш пол (0 - мужчина, 1 - женщина): " << endl;
    while (!(cin >> sex))
    {
        cout << "Некорректный ввод!" << endl;
        clearCin();
        cout << "Введите ваш пол (0 - мужчина, 1 - женщина): " << endl;
    }
    data.insert({"sex", sex ? "1" : "0"});

    while (true) {
        cout << "Введите ваш СНИЛС: " << endl;
        while (!(cin >> snils))
        {
            cout << "Некорректный ввод!" << endl;
            clearCin();
            cout << "Введите ваш СНИЛС: " << endl;
        }

        if (countLongInt(snils) == 11)
            break;
        else
            cout << "В СНИЛСе должно быть 11 цифр." << endl;
    }

    if (dbFile.getLineByValue("snils", to_string(snils)) != -1)
    {
        cout << "Такой пациент уже зарегестрирован!" << endl;
        return;
    }
    data.insert({"snils", to_string(snils)});

    while (true) {
        cout << "Введите ваш ОМС: " << endl;
        while (!(cin >> oms))
        {
            cout << "Некорректный ввод!" << endl;
            clearCin();
            cout << "Введите ваш ОМС: " << endl;
        }

        if (countLongInt(oms) == 16)
            break;
        else
            cout << "В ОМС должно быть 16 цифр." << endl;
    }
    data.insert({"oms", to_string(oms)});

    dbFile.push(data);
    cout << "Пациент добавлен!" << endl;
}

void makeAppointment() {
    string specialist, name, snils;
    DbFile dbWorkers("workers.csv", Headers::getWorkers()),
        dbShedule("shedule.csv", Headers::getShedule()),
        dbVisits("visits.csv", Headers::getVisits()),
        dbPatients("patients.csv", Headers::getPatients());

    cout << "К какому специалисту Вы хотите записатьтся на прием?" << endl;
    clearCin();
    getline(cin, specialist);

    // запрос к бд
    auto query = [specialist](map<string, string> line) {
        return line["specialist"] == specialist
               && line["dismissed"] == "0"
               && line["pass"] == "0";
    };

    // поиск
    auto found = dbWorkers.find(query);

    if (found.empty())
    {
        cout << "Специалисты не найдены" << endl;
        return;
    }

    cout << "Найдено " << found.size() << ":" << endl;
    for (auto line : found)
        cout << dbWorkers[line]["name"] << endl;

    cout << "Введите ФИО необходимого специалиста:" << endl;
    clearCin();
    getline(cin, name);

    int id = -1;

    // поиск специалиста
    for (auto line : found)
        if (dbWorkers[line]["name"] == name)
        {
            id = line;
            break;
        }

    if (id == -1)
    {
        cout << "Такого специалиста нет." << endl;
        return;
    }

    // поиск записей к этому специалисту
    vector<size_t> found_visits;
    found_visits = dbVisits.find([id](map<string, string> line) {
        return line["doctorid"] == to_string(id);
    });

    int added, week, day;
    string start, end;

    if (found_visits.empty())
    {
        added = 0;
        week = _week;
        day = 0;
    }
    else
    {
        auto line = dbVisits[found_visits.back()];
        
        added = stoi(line["time"]);
        day = stoi(line["day"]);
        week = stoi(line["week"]);
    }

    int minutes, hours, _minutes, _hours;

    int line_shedule = dbShedule.getLineByValue("id", to_string(id));
    if (line_shedule == -1 || dbShedule[line_shedule]["accept"] == "0")
    {
        cout << "Данный специалист сейчас не принимает" << endl;
        return;
    }

    auto shedule = dbShedule[line_shedule];

    if (day == 0)
    {
        for (int i = 1; i < 7; ++i) {
            if (shedule[to_string(i)] != "0")
            {
                day = i;
                break;
            }
        }
    }

    Time::convertTime(shedule[to_string(day)].substr(6, 5), _hours, _minutes);
    Time::convertTime(shedule[to_string(day)].substr(0, 5), hours, minutes);

    added += 20;
    Time::addTime(added, hours, minutes);

    if (Time::compareTime(hours, minutes, _hours, _minutes))
    {
        day = 0;

        for (int i = day; i < 7; ++i) {
            if (shedule[to_string(i)] != "0")
            {
                day = i;
                break;
            }
        }

        if (day == 0)
        {
            week += 1;

            for (int i = 1; i < 7; ++i) {
                if (shedule[to_string(i)] != "0")
                {
                    day = i;
                    break;
                }
            }
        }

        Time::convertTime(shedule[to_string(day)].substr(6, 5), _hours, _minutes);
        Time::convertTime(shedule[to_string(day)].substr(0, 5), hours, minutes);

        added = 20;
        Time::addTime(added, hours, minutes);
    }

    cout << "Пациент будет записан на " << Time::getString(hours, minutes) << " на " << week + 1 << " неделе " << endl;
    cout << "Введите СНИЛС пациента: " << endl;
    clearCin();
    cin >> snils;

    if (dbPatients.getLineByValue("snils", snils) == -1)
    {
        cout << "Такой пациент не найден" << endl;
        return;
    }

    map<string,string> data{
            {"doctorid", to_string(id)},
            {"visitorid", snils},
            {"day", to_string(day)},
            {"time", to_string(added)},
            {"week", to_string(week)}
    };

    dbVisits.push(data);
    cout << "Успешно записали на прием" << endl;
}

void hireEmployee() {
    string name, specialist;
    bool isDoctor;
    int cabinet, id, candidate;
    map<string, string> data;
    DbFile dbFile("workers.csv", Headers::getWorkers());

    cout << "Введите ФИО нового сотрудника: " << endl;
    clearCin();
    getline(cin, name);
    data.insert({"name", name});

    cout << "Он доктор? (0 - нет, 1 - да): " << endl;
    while (!(cin >> isDoctor))
    {
        cout << "Некорректный ввод!" << endl;
        clearCin();
        cout << "Он доктор? (0 - нет, 1 - да): " << endl;
    }
    data.insert({"isdoctor", isDoctor ? "1" : "0"});

    cout << "Должность нового сотрудника: " << endl;
    clearCin();
    getline(cin, specialist);
    data.insert({"specialist", specialist});

    candidate = dbFile.getLineByValue("name", name);
    if (candidate != -1 && dbFile[candidate]["specialist"] == specialist)
    {
        cout << "Такой сотрудник уже существует!" << endl;
        return;
    }

    cout << "Введите кабинет сотрудника (-1, если его нет): " << endl;
    while (!(cin >> cabinet))
    {
        cout << "Некорректный ввод!" << endl;
        clearCin();
        cout << "Введите кабинет сотрудника (-1, если его нет): " << endl;
    }
    data.insert({"cabinet", to_string(cabinet)});
    data.insert({"dismissed", "0"});
    data.insert({"pass", "0"});

    id = dbFile.getLast() + 1;
    data.insert({"id", to_string(id)});

    dbFile.push(data);
    cout << "Сотрудник добавлен!" << endl;
};

void sendOnVacation() {
    string id, reason;
    int end, db_line;
    DbFile dbWorkers("workers.csv", Headers::getWorkers()),
        dbPass("pass.csv", Headers::getPass());
    map<string, string> data;

    cout << "Введите id сотрудника:" << endl;
    cin >> id;

    db_line = dbWorkers.getLineByValue("id", id);

    if (db_line== -1)
    {
        cout << "Сотрудник не найден" << endl;
        return;
    }
    data.insert({"id", id});

    auto line = dbWorkers[db_line];
    line["pass"] = "1";
    dbWorkers.writeLine(db_line, line);

    cout << "Какова причина отсутствия сотрудника?" << endl;
    cin >> reason;
    data.insert({"reason", reason});

    cout << "На какой неделе закончится отпуск: " << endl;
    while (!(cin >> end))
    {
        cout << "Некорректный ввод!" << endl;
        clearCin();
        cout << "На какой неделе закончится отпуск:" << endl;
    }
    data.insert({"end", to_string(end)});

    dbPass.push(data);
};

void dismissEmployee() {
    string id, reason;
    int db_line;
    DbFile dbWorkers("workers.csv", Headers::getWorkers()),
        dbVisits("visits.csv", Headers::getVisits()),
        dbShedule("shedule.csv", Headers::getShedule()),
        dbPass("pass.csv", Headers::getPass());
    map<string, string> data;

    cout << "Введите id сотрудника:" << endl;
    cin >> id;

    db_line = dbWorkers.getLineByValue("id", id);

    if (db_line == -1)
    {
        cout << "Сотрудник не найден" << endl;
        return;
    }
    data.insert({"id", id});

    auto found = dbVisits.find([id](map<string,string> line) { return line["doctorid"] == id; });
    if (!found.empty() && stoi(dbVisits[found.back()]["week"]) >= _week)
    {
        cout << "Обслужите всех пациентов на этой неделе, потом сделайте повторный запрос на следующей неделе." << endl;
        cout << "Возможность записаться на прием к данному сотруднику отключена." << endl;

        auto line = dbWorkers[db_line];
        line["dismissed"] = "1";
        dbWorkers.writeLine(db_line, line);
        return;
    }

    dbWorkers.pop(db_line);

    db_line = dbShedule.getLineByValue("id", id);
    if (db_line != -1)
        for (size_t index : found)
            dbVisits.pop(index);

    db_line = dbPass.getLineByValue("id", id);
    if (db_line != -1)
        dbPass.pop(db_line);

    cout << "Сотрудник уволен!" << endl;
}

void showShedule(bool doctors_only = false) {
    DbFile dbShedule("shedule.csv", Headers::getShedule()), dbWorkers("workers.csv", Headers::getWorkers());
    const string days[] = {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"};
    cout << "Расписание: " << endl;

    for (int i = 0; i < dbShedule.size(); ++i) {
        auto line_shedule = dbShedule[i];
        int line_workers = dbWorkers.getLineByValue("id", line_shedule["id"]);

        if (line_workers == -1)
            continue;

        if (doctors_only && dbWorkers[line_workers]["isdoctor"] != "1")
            continue;

        cout << dbWorkers[line_workers]["id"] << ". " << dbWorkers[line_workers]["name"] << " (" << dbWorkers[line_workers]["specialist"] << "):" << endl;
        for (int j = 1; j < 7; ++j) {
            if (line_shedule[to_string(j)] == "0")
                continue;

            cout << "- " << days[j - 1] << ": " << line_shedule[to_string(j)] << endl;
        }

        cout << endl;
    }
}

void editShedule()
{
    string id, period;
    const string days[] = {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"};
    int db_line;
    DbFile dbWorkers("workers.csv", Headers::getWorkers()),
            dbShedule("shedule.csv", Headers::getShedule());
    map<string, string> data;
    bool changed = false, accept;

    cout << "Введите id сотрудника:" << endl;
    cin >> id;

    db_line = dbWorkers.getLineByValue("id", id);

    if (db_line == -1)
    {
        cout << "Сотрудник не найден" << endl;
        return;
    }
    data.insert({"id", id});

    for (int i = 1; i < 7; ++i) {
        while (true)
        {
            cout << "Введите режим работы для " << days[i-1] << " (формат: HH:MM-HH:MM) или 0, если выходной:" << endl;
            cin >> period;

            if (period == "0")
                break;

            try {
                int hours, minutes, _hours, _minutes, in_minutes, _in_minutes;

                Time::convertTime(period.substr(0, 5), hours, minutes);
                Time::convertTime(period.substr(6, 5), _hours, _minutes);

                in_minutes = Time::getMinutes(hours, minutes);
                _in_minutes = Time::getMinutes(_hours, minutes);

                if (_in_minutes - in_minutes <= 0)
                    cout << "Задан неверный интервал времени" << endl;

                break;
            } catch (const exception& e) {
                cout << "Некорректно задан интервал! Пример правильного интервала: 08:00-10:30" << endl;
            }
        }

        data.insert({to_string(i), period});
    }

    for (auto & itr : data)
        if (!changed && itr.second != "0")
            changed = true;

    if (!changed) {
        cout << "Данное расписание бессмысленно." << endl;
        return;
    }

    cout << "К нему можно записаться (0 - нет, 1 - да): " << endl;
    while (!(cin >> accept))
    {
        cout << "Некорректный ввод!" << endl;
        clearCin();
        cout << "К нему можно записаться (0 - нет, 1 - да): " << endl;
    }
    data.insert({"accept", accept ? "1" : "0"});

    db_line = dbShedule.getLineByValue("id", id);

    if (db_line != -1)
        dbShedule.writeLine(db_line, data);
    else
        dbShedule.push(data);

    cout << "Расписание изменено." << endl;
}

void updatePass() {}

int main() {
    int input;

    setlocale(LC_ALL, "Russian");

    while (true) {
        cout << "Для начала работы введите номер недели: " << endl;
        while (!(cin >> _week))
        {
            cout << "Некорректный ввод!" << endl;
            clearCin();
            cout << "Для начала работы введите номер недели: " << endl;
        }

        if (_week >= 0)
            break;
        else
            cout << "Номер недели не может быть отрицательным." << endl;
    }

    cout << "Здравствуйте. Для начала работы выберите необходимый пункт" << endl;
    writeMenu();

    while (true) {
        cout << "Напишите номер необходимого пункта и нажмите Enter: " << endl;
        try {
            if (!(cin >> input)){
                cout << "Некорректный ввод! Нужно вводить только целые числа." << endl;
                clearCin();
                continue;
            }

            switch (input) {
                case 1:
                    registerPatients();
                    break;
                case 2:
                    makeAppointment();
                    break;
                case 3:
                    hireEmployee();
                    break;
                case 4:
                    dismissEmployee();
                    break;
                case 5:
                    sendOnVacation();
                    break;
                case 6:
                    showShedule();
                    break;
                case 7:
                    showShedule(true);
                    break;
                case 8:
                    editShedule();
                    break;
                case 9:
                    updatePass();
                    break;
                case 0:
                    return 0;
                default:
                    cout << "Данного пункта нет в меню!" << endl;
                    break;
            }

            writeMenu();
        } catch (const exception &e) {
            cout << "Произошла непредвиденаня ошибка (" << e.what() << ")" << endl;
            break;
        }
    }

    return 0;
}

